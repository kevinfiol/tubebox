import env from 'env-smart';
env.load();

const PORT = /** @type number **/ (/** @type unknown **/ (process.env.PORT));
const DB_PORT = /** @type number **/ (/** @type unknown **/ (process.env.DB_PORT))

/** @type import('./src/types').Config **/
const config = {
    mode: process.env.MODE,
    port: PORT,
    origin: process.env.ORIGIN,
    hashql_url: process.env.HASHQL_URL,
    api_url: process.env.API_URL,
    youtube_api_key: process.env.YOUTUBE_API_KEY,
    postgres: {
        host: process.env.DB_HOST,
        port: DB_PORT,
        database: process.env.DB_NAME,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD
    }
};

export default config;