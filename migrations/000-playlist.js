export async function up(sql) {
    await sql`
        create table if not exists playlist (
            id serial primary key,
            name text unique,
            video_ids text,
            created_at timestamp not null
        );
    `;
};

export async function down(sql) {
    await sql`
        drop table if exists playlist;
    `;
};