export async function up(sql) {
    await sql`
        create table if not exists video (
            id serial primary key,
            name text,
            youtube_id text,
            created_at timestamp not null
        )
    `;
};

export async function down(sql) {
    await sql`
        drop table if exists video;
    `;
};