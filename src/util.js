/**
 * @param {string} tag
 * @param {...string} classes
 * @returns {string} str
 */
export const sel = (tag, ...classes) => {
    let str = tag;
    for (let i = 0, len = classes.length; i < len; i++) {
        classes[i] && (str += '.' + classes[i].trim());
    }
    return str;
}

/**
 * adapted from https://github.com/schollz/rwtxt
 * https://github.com/schollz/rwtxt/blob/master/LICENSE
 * 
 * @param {string} text
 * @returns {string} slug
 */
export const slugify = (text) => {
    let lines = text.split('\n');

    for (let i = 0; i < lines.length; i++) {
        const slug = lines[i].toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, '') // Trim - from end of text
        ;

        if (slug.length > 0)
            return slug;
    }

    return '';
}

export const post = (endpoint, body) =>
    fetch(endpoint, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body)
    }).then((res) => res.json());