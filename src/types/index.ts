import { ServerResponse, IncomingMessage, RequestListener } from 'http';

// Client
export type State = {
    route: string;
    isLoading: boolean;
};

export type Actions = {
    setRoute: (route: string, params?: Record<string, any>) => void;
    setIsLoading: (isLoading: boolean) => void;
};

export type StateFactory = () => State;
export type ActionsFactory = (state: State) => Actions;

// Server
export type Config = {
    mode: string;
    port: number;
    origin: string;
    api_url: string;
    hashql_url: string;
    youtube_api_key: string;
    postgres: import('postgres/types').Options<any>
};

export type HQLTag = (
    query: string[],
    input: string[],
    context: string
) => any;

export type EyHandler = (
    req: IncomingMessage & { body: any },
    res: ServerResponse,
    next: () => void
) => void;

export type EyApp = RequestListener & {
    use: (...middleware: EyHandler[]) => EyApp;
    post: (route: string, ...middleware: EyHandler[]) => EyApp;
    get: (route: string, ...middleware: EyHandler[]) => EyApp;
};