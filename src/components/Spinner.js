import m from 'mithril';

export const Spinner = () => {
    let step = 0;
    let spinner = '/';
    let timer;

    return {
        oninit: () => {
            timer = setInterval(() => {
                m.redraw();
                if (step === 7) {
                    spinner = '|';
                    step = 0;
                    return;
                }
                if (step === 0 || step === 4)
                    spinner = '/';
                if (step === 1 || step === 5)
                    spinner = '-';
                if (step === 2 || step === 6)
                    spinner = '\\';
                if (step === 3)
                    spinner = '|';
                step += 1;
            }, 100);
        },

        onremove: () => {
            clearInterval(timer);
        },

        /** @param {{ children: any[] }} vnode **/
        view: ({ children }) =>
            m('div.spinner',
                m('span', spinner),
                m('span', children)
            )
    };
};