import m from 'mithril';

/**
 * @typedef {object} FormAttrs
 * @property {(ev: SubmitEvent) => void} onSubmit
 */

export const Form = () => ({
    /** @param {{ children: any, attrs: FormAttrs }} vnode **/
    view: ({ attrs: { onSubmit }, children }) =>
        m('form.form', {
            onsubmit: (ev) => {
                ev.preventDefault();
                onSubmit(ev);
            }
        }, children)
});