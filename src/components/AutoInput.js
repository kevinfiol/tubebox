import m from 'mithril';
import Etto from 'etto';

/**
 * @typedef {object} AutoInputAttrs
 * @property {string} initialValue
 * @property {Record<string, unknown>} config
 * @property {Record<string, unknown>[]} choices
 */

export const AutoInput = () => {
    let ettoContainer = null;
    let etto = null;

    return {
        /** @param {{ dom: HTMLElement, attrs: AutoInputAttrs }} vnode **/
        oncreate: ({ dom, attrs: { initialValue, config, choices } }) => {
            ettoContainer = dom;
            etto = new Etto(
                ettoContainer,
                { ...(config || {}), classList: ['input'] },
                choices
            );

            etto.value = initialValue || '';
        },

        ondestroy: () => {
            etto.destroy();
            etto = undefined;
            ettoContainer = undefined;
        },

        view: () => m('div')
    };
};
