import m from 'mithril';

export const InputGroup = () => ({
    /** @param {{ children: any[] }} vnode **/
    view: ({ children }) =>
        m('div.input-group.flex', children)
});