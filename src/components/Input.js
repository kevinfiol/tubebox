import m from 'mithril';

/**
 * @typedef {object} InputAttrs
 * @property {string} placeholder
 * @property {string} value
 * @property {(value: string) => void} onInput
 */

export const Input = () => ({
    /** @param {{ attrs: InputAttrs }} vnode **/
    view: ({ attrs: { placeholder, value, onInput } }) =>
        m('input.input', {
            placeholder,
            value,
            oninput: ({ target }) => onInput(target.value)
        })
});