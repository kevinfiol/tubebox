import http from 'http';
import { createRequire } from 'module';
import ey from 'ey';
import { json } from '@polka/parse';
import postgres from 'postgres';
import HashQL from 'hashql/server.js';
import { YouTube } from 'popyt';
import config from '../config.js';

// @ts-ignore
const require = createRequire(import.meta.url);
const DEV = config.mode == 'dev';
const PORT = config.port;
const ORIGIN = DEV ? '*' : config.origin;

const sql = postgres(config.postgres);
const hql = HashQL(DEV ? x => x : require('./queries.json'), {
    /** @type import('./types').HQLTag **/
    sql: (query, input, _context) => {
        return sql.call(null, query, ...input);
    }
});

// youtube api setup
const youtube = new YouTube(config.youtube_api_key);

/** @type import('./types').EyApp **/
const app = ey();

const optionsHandler = (_req, res) => {
    res.writeHead(200, {
        'Access-Control-Allow-Origin': ORIGIN,
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': 86400
    });

    res.end();
};

app.use(
    json(),
    (req, _, next) => {
        console.log(`~> [${req.method}] ${req.url}`);
        next();
    }
)
.options('/hql', optionsHandler)
.options('/searchYoutube', optionsHandler)
.post('/hql', async (req, res) => {
    const payload = { data: { rows: [] }, error: null };
    let statusCode = 200;

    try {
        const rows = await hql(req.body);
        payload.data.rows = rows;
    } catch (e) {
        console.error(e);
        payload.error = e.message;
        statusCode = e.statusCode || 500;
    }

    res.writeHead(statusCode, {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': ORIGIN,
        'Vary': 'Origin'
    });

    res.write(JSON.stringify(payload));
    res.end()
}).post('/searchYoutube', async (req, res) => {
    /** @type {{ query: string }} **/
    const { query } = req.body;
    const payload = { data: { videos: [] }, error: null };
    let statusCode = 200;

    try {
        if (!query) throw Error('Method requires `query` parameter.');

        const search = await youtube.searchVideos(query.trim(), 12);
        let videos = [];

        if (search && search.results) {
            videos = search.results
                .map((item) => ({
                    url: item.url,
                    id: item.id,
                    title: item.title,
                    thumbnail: item.thumbnails.default
                }))
        }

        payload.data.videos = videos;
    } catch (e) {
        console.error(e);
        payload.error = e.message;
        statusCode = e.statusCode || 500;
    }

    res.writeHead(statusCode, {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': ORIGIN,
        'Vary': 'Origin'
    });

    res.write(JSON.stringify(payload));
    res.end();
});

http.createServer(app).listen(PORT);
if (DEV) console.log('Running server in *DEV* mode.');
console.log(`Listening on port: ${PORT}`);