import m from 'mithril';

/**
 * @typedef {import('./types').StateFactory} StateFactory
 * @typedef {import('./types').ActionsFactory} ActionsFactory
 */

/** @type {StateFactory} **/
const State = () => ({
    route: '/',
    isLoading: false
});

/** @type {ActionsFactory} */
const Actions = (state) => ({
    setRoute: (route, params) => {
        state.route = route;
        m.route.set(route, params || undefined);
    },

    setIsLoading: (isLoading) => {
        state.isLoading = isLoading;
    }
});

export { State, Actions };