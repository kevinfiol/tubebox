import m from 'mithril';
import sql from '../sql';
import { slugify } from '../util';
import { Form } from '../components/Form';
import { Input } from '../components/Input';
import { InputGroup } from '../components/InputGroup';

/**
 * @typedef {object} IndexAttrs
 * @property {import('../types').Actions} actions
 */

/** @param {{ attrs: IndexAttrs }} vnode **/
export const Index = ({ attrs: { actions } }) => {
    let playlistName = '';
    let errorMsg = '';
    let isPlaylistCreated = false;
    let createdName = null;

    const createPlaylist = async () => {
        errorMsg = '';
        if (!playlistName.trim()) return;
        actions.setIsLoading(true);

        const slug = slugify(playlistName);

        const { data } = await sql`
            select id from playlist
            where name = ${slug}
        `;

        if (data.rows.length) {
            errorMsg = 'playlist already exists';
        } else {
            const { error } = await sql`
                insert into playlist (name, video_ids, created_at)
                values (${slug}, '', now());
            `;

            if (error) {
                errorMsg = `error creating playlist ${playlistName}`;
            } else {
                createdName = playlistName.trim();
                isPlaylistCreated = true;
                actions.setRoute('/' + slug);
            }
        }

        actions.setIsLoading(false);
        m.redraw();
    };

    return {
        view: () =>
            m('div',
                m('h1', 'tubebox'),
                m('section',
                    m(Form, { onSubmit: createPlaylist },
                        m(InputGroup,
                            m(Input, {
                                placeholder: 'what\'s your playlist called?',
                                value: playlistName,
                                onInput: (value) => playlistName = value
                            }),

                            m('button', {
                                type: 'submit',
                                disabled: !playlistName.trim()
                            }, 'create')
                        )
                    ),

                    errorMsg &&
                        m('blockquote.m-0', errorMsg)
                    ,

                    isPlaylistCreated &&
                        m('blockquote.m-0',
                            `Playlist ${createdName} has been created.`
                        )
                    ,
                )
            )
    };
};