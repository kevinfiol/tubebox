import m from 'mithril';
import sql from '../sql';
import { post } from '../util';
import { AutoInput } from '../components/AutoInput';

const YT_SEARCH_ENDPOINT = process.env.API_URL + 'searchYoutube';

/**
 * @typedef {object} PlaylistAttrs
 * @property {import('../types').Actions} actions
 * @property {string} slug
 */

/** @param {{ attrs: PlaylistAttrs }} vnode **/
export const Playlist = ({ attrs: { slug } }) => {
    return {
        /** @param {{ attrs: PlaylistAttrs }} vnode **/
        oninit: async ({ attrs: { actions, slug } }) => {
            // check if playlist exists
            const { data, error } = await sql`
                select id from playlist
                where name = ${slug}
            `;

            if (error || !data.rows.length) {
                m.route.set('/');
            }
        },

        view: () =>
            m('div',
                m('h1', slug),
                m('section',
                    m('p', 'welcome to my section'),
                    m(AutoInput, {
                        config: {
                            placeholder: 'search for videos',
                            source: searchSource,
                            filterFn: (_val, choices) => choices
                        }
                    })
                )
            )
    };
};

let searchController;

async function searchSource(query, done) {
    if (searchController) searchController.abort();
    searchController = new AbortController();

    try {
        const { data } = await post(YT_SEARCH_ENDPOINT, { query });
        searchController = null;

        const choices = (data && data.videos)
            ? data.videos.map((video) => ({ label: video.title, value: video.id }))
            : [];

        done(choices);
    } catch (e) {
        console.error(e);
        if (e.name !== 'AbortError') done([]);
    }
}