import m from 'mithril';
import { Spinner } from '../components/Spinner';

/**
 * @typedef {object} LayoutAttrs
 * @property {import('../types').State} state
 */

export const Layout = () => ({
    /** @param {{ children: any[], attrs: LayoutAttrs }} vnode **/
    view: ({ attrs: { state }, children }) =>
        m('main',
            state.isLoading &&
                m(Spinner)
            ,

            children
        )
});