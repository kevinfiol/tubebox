import HashQL from 'hashql'
import { post } from './util';

const ENDPOINT = process.env.HASHQL_URL;
const { sql } = HashQL('sql', query => post(ENDPOINT, query));

export default sql;