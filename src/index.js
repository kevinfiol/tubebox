import m from 'mithril';
import { State, Actions } from './state';
import { Layout } from './pages/Layout';
import { Index } from './pages/Index';
import { Playlist } from './pages/Playlist';

const state = State();
const actions = Actions(state);

m.route.prefix = '';
m.route(document.getElementById('app'), '/', {
    '/': {
        render: () =>
            m(Layout, { state },
                m(Index, { state, actions })
            )
    },

    '/:slug': {
        /** @param {{ attrs: { slug: string } }} vnode **/
        render: ({ attrs: { slug } }) =>
            m(Layout, { state },
                m(Playlist, { state, actions, slug })
            )
    }
})