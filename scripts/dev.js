import servbot from 'servbot';
import { bundle, logSuccess, logError } from './bundle.js';

const server = servbot({
    root: 'dist',
    reload: true,
    fallback: 'index.html'
});

server.listen(5000);

bundle({
    minify: false,
    sourcemap: true,
    watch: {
        onRebuild(error) {
            if (error) logError(error);
            else logSuccess();
            server.reload();
        }
    }
}).then(logSuccess).catch(e => {
    logError(e);
    process.exit(1);
});
